import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
import numpy as np
from numpy import ravel
from keras.layers import Input
from keras.models import Model
from keras.models import Sequential
from keras.layers import Dense, Activation
from keras import regularizers,optimizers
from sklearn import svm
import time
import theano
import theano.tensor as T
from unittest.mock import Mock

import lasagne
#from sknn import mlp
#from matplotlib import pyplot
from sklearn.model_selection import cross_val_score

def normalize_train(input_arr):
    eps = 0.00001
    mean = np.mean(input_arr,axis=0)
    adjusted = input_arr - mean
    std = np.std(adjusted,axis=0)
    std = std+(np.abs(std) < eps)
    normed = adjusted/std
    return normed,mean,std

def normalize_test(input_arr,means,stds):
    adjusted = input_arr - means
    normed = adjusted/stds
    return normed

def build_mlp(input_var=None,input_shape=(),hidden_dim=()):
    input_new_shape = (None,input_shape[1])
    l_in = lasagne.layers.InputLayer(shape=input_new_shape,
                                     input_var=input_var)
    l_in_drop = lasagne.layers.DropoutLayer(l_in, p=0.2)
    l_hid1 = lasagne.layers.DenseLayer(
        l_in_drop, num_units=hidden_dim,
        nonlinearity=lasagne.nonlinearities.rectify,
        W=lasagne.init.GlorotUniform())
    l_hid1_drop = lasagne.layers.DropoutLayer(l_hid1, p=0.5)

    l_hid2 = lasagne.layers.DenseLayer(
        l_hid1_drop, num_units=600,
        nonlinearity=lasagne.nonlinearities.rectify)

    l_hid2_drop = lasagne.layers.DropoutLayer(l_hid2, p=0.5)
    l_out = lasagne.layers.DenseLayer(
        l_hid1_drop, num_units=4,
        nonlinearity=lasagne.nonlinearities.softmax)
    return l_out

def build_mlp_fns(network,input_var,target_var):
    prediction = lasagne.layers.get_output(network)
    loss = lasagne.objectives.categorical_crossentropy(prediction, target_var)
    loss = loss.mean()

    params = lasagne.layers.get_all_params(network, trainable=True)
    updates = lasagne.updates.nesterov_momentum(
        loss, params, learning_rate=0.01, momentum=0.9)

    test_prediction = lasagne.layers.get_output(network, deterministic=True)
    test_loss = lasagne.objectives.categorical_crossentropy(test_prediction,
                                                            target_var)
    test_loss = test_loss.mean()

    test_acc = T.mean(T.eq(T.argmax(test_prediction, axis=1), target_var),
                      dtype=theano.config.floatX)

    train_fn = theano.function([input_var, target_var], loss, updates=updates, allow_input_downcast=True)
    val_fn = theano.function([input_var, target_var], [test_loss, test_acc], allow_input_downcast=True)
    pred_fn = theano.function([input_var],
                              test_prediction,
                              allow_input_downcast=True)
    return train_fn,val_fn,pred_fn

def validate_model(val_fn,data,labels):
    val_err = 0
    val_acc = 0
    val_batches = 0
    for batch in iterate_minibatches(data, labels, 100, shuffle=False):
        inputs, targets = batch
        err, acc = val_fn(inputs, targets)
        val_err += err
        val_acc += acc
        val_batches += 1
    return val_err,val_acc,val_batches

def train_model(train_fn,trainData,trainLabels,testData=[],testLabels=[],num_epochs = 10,val_fn=None):
    train_accuracy=0
    test_accuracy=0
    for epoch in range(num_epochs):
        # In each epoch, we do a full pass over the training data:
        train_err = 0
        train_batches = 0
        start_time = time.time()
        for batch in iterate_minibatches(trainData, trainLabels, 500, shuffle=True):
            inputs, targets = batch
            # print("inp: ", inputs.shape, " target:", targets.shape)
            train_err += train_fn(inputs, targets)
            train_batches += 1


        # And a full pass over the validation data:
        if val_fn:
            val_err, val_acc, val_batches = validate_model(val_fn,testData,testLabels)
            val_train_err, val_train_acc, val_train_batches = validate_model(val_fn,trainData,trainLabels)
            print("  training loss:\t\t{:.6f}".format(train_err / train_batches))
            print("  validation loss:\t\t{:.6f}".format(val_err / val_batches))

            train_accuracy = (val_train_acc / val_train_batches * 100)
            test_accuracy = val_acc/val_batches*100
            print("  train accuracy:\t\t{:.2f} %".format(
                train_accuracy))
            print("  validation accuracy:\t\t{:.2f} %".format(
                test_accuracy))
        # Then we print the results for this epoch:
        print("Epoch {} of {} took {:.3f}s".format(
            epoch + 1, num_epochs, time.time() - start_time))
    return train_accuracy, test_accuracy


def build_AE(input_var=None,input_shape=(),encoding_dim=(),denoising = True):
    print("Building network ...")
    NUM_FEATURES = input_shape[1]
    input_new_shape = (None, NUM_FEATURES)
    # Define the layers
    l_in = lasagne.layers.InputLayer(shape=input_new_shape)
    l_in_d = l_in
    if denoising:
        l_in_d = lasagne.layers.DropoutLayer(l_in,p=0.05)

    encoder_l_out = lasagne.layers.DenseLayer(l_in_d,
                                              num_units=encoding_dim,
                                              W=lasagne.init.Normal(),
                                              nonlinearity=lasagne.nonlinearities.rectify)
    decoder_l_out = lasagne.layers.DenseLayer(encoder_l_out,
                                              num_units=NUM_FEATURES,
                                              W=lasagne.init.Normal(),
                                              nonlinearity=lasagne.nonlinearities.sigmoid)

    # Define some Theano variables
    target_values = theano.tensor.fmatrix('target_output')
    encoded_output = lasagne.layers.get_output(encoder_l_out, deterministic=True)
    network_output = lasagne.layers.get_output(decoder_l_out)

    cost = lasagne.objectives.squared_error(network_output, target_values).mean()
    all_params = lasagne.layers.get_all_params(decoder_l_out, trainable=True)

    # Compute AdaDelta updates for training
    updates = lasagne.updates.adadelta(cost, all_params)

    # Some Theano functions
    train = theano.function([l_in.input_var, target_values],
                            cost,
                            updates=updates,
                            allow_input_downcast=True)
    predict = theano.function([l_in.input_var],
                              network_output,
                              allow_input_downcast=True)
    encode = theano.function([l_in.input_var],
                             encoded_output,
                             allow_input_downcast=True,)

    return train, predict, encode

def train_AE(trainfunc,x_train):
    state_ae = Mock()
    state_ae.NUM_EPOCHS = 10

    for it in range(state_ae.NUM_EPOCHS):

        count = 0
        avg_cost = 0
        for batch in iterate_minibatches(x_train, x_train, 500, shuffle=False):
            x, xl = batch
            count += 1
            avg_cost += trainfunc(x, xl)


        print("Epoch {} average loss = {}".format(it, avg_cost / count))


def iterate_minibatches(inputs, targets, batchsize, shuffle=False):
    last_end = 0
    for end in range(batchsize,len(inputs),batchsize):
        yield inputs[last_end:end], targets[last_end:end]
        last_end = end


feature_dir = "data/export_plus_resnet/"
[trainDataMain,means,stds] =normalize_train(pd.read_csv(feature_dir+"trainData.csv").values)
trainLabelsColumn = pd.read_csv(feature_dir+"trainLabels.csv")
testData = normalize_test(pd.read_csv(feature_dir+"testData.csv").values,means,stds)#[:,(3176-1128):]
testLabelsColumn= pd.read_csv(feature_dir+"testLabels.csv")

trainLabelsMain = ravel(trainLabelsColumn)
testLabels = ravel(testLabelsColumn)

train_sizes = range(18000,np.size(trainLabelsMain),2000)
accTrains = []
accTests = []

train_sizes = [np.size(trainLabelsMain)]
for train_size in train_sizes:
    print("Applying trainsize: "+str(train_size))
    trainData=trainDataMain[0:train_size]#,(3176-1128):]
    trainLabels = trainLabelsMain[0:train_size]
    method = 4
    if method == 1:

        clf = RandomForestClassifier()

        #scores = cross_val_score(clf,trainData,trainLabels)
        clf.fit(trainData,trainLabels)
        trainPred = clf.predict(trainData)
        train_accuracy=(accuracy_score(trainPred,trainLabels))

        testPred = clf.predict(testData)
        test_accuracy=(accuracy_score(testPred,testLabels))
    elif method == 2:
        print(pd.DataFrame(trainData).shape)
        input_dim = trainData.shape[1]
        encoding_dim = 256
        hidden_dim = 90
        input_buf = Input(shape=(input_dim,))
        encoded = Dense(encoding_dim, activation="relu")(input_buf)
        decoded = Dense(input_dim, activation="sigmoid")(encoded)
        autoencoder = Model(input_buf, decoded)
        encoder = Model(input_buf, encoded)
        autoencoder.compile(optimizer='sgd', loss='binary_crossentropy')
        autoencoder.fit(trainData, trainData,
                        epochs=10,
                        batch_size=256,
                        shuffle=True,
                        validation_data=(trainData, trainData))

        print("Learning classifier")

        multilp = Sequential([
            Dense(hidden_dim, input_shape=(encoding_dim,)),
            Activation('relu'),
            Dense(4),
            Activation('softmax'),
        ])

        trainOneHot = list(map((lambda x: [0 if i != (x - 1) else 1 for i in range(4)]), trainLabels))

        encodedTrainData = encoder.predict(trainData)
        encodedTestData = encoder.predict(testData)

        multilp.compile(optimizer='sgd',
                        loss='categorical_crossentropy',
                        metrics=['accuracy'])

        multilp.fit(encodedTrainData, trainOneHot)

        trainPred = multilp.predict_classes(encodedTrainData)
        testPred = multilp.predict_classes(encodedTestData)

        train_accuracy = (accuracy_score(trainPred, trainLabels))
        test_accuracy = (accuracy_score(testPred, testLabels))
    elif method == 3:
        print(pd.DataFrame(trainData).shape)
        input_dim = trainData.shape[1]
        encoding_dim = 30
        hidden_dim = 10
        input_buf = Input(shape= (input_dim,))
        encoded = Dense(encoding_dim,activation="relu")(input_buf)
        decoded = Dense(input_dim, activation="sigmoid")(encoded)
        autoencoder = Model(input_buf,decoded)
        encoder = Model(input_buf,encoded)
        autoencoder.compile(optimizer='adadelta', loss='binary_crossentropy')
        autoencoder.fit(trainData, trainData,
                        epochs=10,
                        batch_size=256,
                        shuffle=True,
                        validation_data=(trainData, trainData))

        print("Learning classifier")

        multilp = Sequential([
            Dense(hidden_dim, input_shape=(encoding_dim,)),
             ('relu'),
            Dense(4),
            Activation('softmax'),
        ])

        trainOneHot=list(map((lambda x: [0 if i != (x-1) else 1 for i in range(4)]),trainLabels))

        encodedTrainData = encoder.predict(trainData)
        encodedTestData =  encoder.predict(testData)

        multilp.compile(optimizer='rmsprop',
                      loss='categorical_crossentropy',
                      metrics=['accuracy'])

        multilp.fit(encodedTrainData,trainOneHot)

        trainPred=multilp.predict_classes(encodedTrainData)
        testPred=multilp.predict_classes(encodedTestData)

        train_accuracy=(accuracy_score(trainPred,trainLabels))
        test_accuracy=(accuracy_score(testPred,testLabels))
    elif method == 4:
        clf = svm.SVC()

        #scores = cross_val_score(clf,trainData,trainLabels)
        clf.fit(trainData,trainLabels)
        trainPred = clf.predict(trainData)
        train_accuracy=(accuracy_score(trainPred,trainLabels))

        testPred = clf.predict(testData)
        test_accuracy=(accuracy_score(testPred,testLabels))
    elif method == 5:
        lambdas = 0.0001
        eta = 0.0005
        print("Single Layer Perceptron")
        print(pd.DataFrame(trainData).shape)
        input_dim = trainData.shape[1]
        # encoding_dim = 30
        # hidden_dim = 10
        output_dim=1

        print("Learning classifier")

        slp = Sequential([
            Dense(output_dim,kernel_regularizer= regularizers.l2(lambdas),input_shape=(input_dim,)),
            Activation('softmax')
        ])

        trainOneHot = np.array(list(map((lambda x: x > 2 ),trainLabels)))#[0 if i != (x - 1) else 1 for i in range(output_dim)]), trainLabels))
        testOneHot = np.array(list(map((lambda x: x > 2 ),testLabels)))#[0 if i != (x - 1) else 1 for i in range(output_dim)]), trainLabels))

        sgd = optimizers.SGD(lr = eta)
        slp.compile(optimizer=sgd,
                        loss='binary_crossentropy',
                        metrics=['accuracy'])

        slp.fit(trainData, trainOneHot)

        train_out = slp.predict(trainData)
        trainPred = slp.predict_classes(trainData)
        testPred = slp.predict_classes(testData)

        train_accuracy = (accuracy_score(trainPred, trainOneHot))
        test_accuracy = (accuracy_score(testPred, testOneHot))
    elif method == 6:
        print(pd.DataFrame(trainData).shape)
        input_dim = trainData.shape[1]
        input_shape= trainData.shape
        hidden_dim = 2048


        zbTrainLabels = np.array(list(map((lambda x:x-1),trainLabels)))
        zbTestLabels = np.array(list(map((lambda x:x-1),testLabels)))
        # Prepare Theano variables for inputs and targets
        input_var = T.matrix('inputs')
        target_var = T.ivector('targets')
        # Create neural network model
        network = build_mlp(input_var,input_shape,hidden_dim)
        train_fn,val_fn,pred_fn = build_mlp_fns(network,input_var,target_var)

        train_accuracy, test_accuracy = train_model(train_fn,trainData,zbTrainLabels,testData,zbTestLabels,val_fn=val_fn)
    elif method == 7:
        print(pd.DataFrame(trainData).shape)
        input_dim = trainData.shape[1]
        encoding_dim = 1024
        hidden_dim = 1024
        input_shape = trainData.shape
        mlp_input_shape = (trainData.shape[0],encoding_dim)

        # Prepare Theano variables for inputs and targets
        encoder_input_var = T.matrix('inputs')

        ea_train_fn,ea_pred_fn,ea_encode_fn=build_AE(encoder_input_var,input_shape,encoding_dim)
        train_AE(ea_train_fn,trainData)

        encodedTrainData = ea_encode_fn(trainData)
        encodedTestData = ea_encode_fn(testData)

        input_var = T.matrix('inputs')
        target_var = T.ivector('targets')

        zbTrainLabels = np.array(list(map((lambda x: x - 1), trainLabels)))
        zbTestLabels = np.array(list(map((lambda x: x - 1), testLabels)))
        # Prepare Theano variables for inputs and targets
        # Create neural network model
        network = build_mlp(input_var, mlp_input_shape, hidden_dim)
        train_fn, val_fn, pred_fn = build_mlp_fns(network, input_var, target_var)

        train_accuracy, test_accuracy = train_model(train_fn, encodedTrainData, zbTrainLabels, encodedTestData, zbTestLabels,
                                                    val_fn=val_fn)


    accTrains += [train_accuracy]
    accTests += [test_accuracy]

print(train_size)
print(accTrains)
print(accTests)
#pyplot.plot(train_sizes,accTrains,"r",train_sizes,accTests,"b")
#pyplot.show()
#print(scores.mean())