import numpy as np

import models.sequential_cnn.models
from consts import *
from features import prepare_inputs

consts = get_consts()

def create_model():
    if consts.model_type == "sequential":
        return models.sequential_cnn.models.create_sequential_model(consts)


def train_classifier_batch(model,X_batch,Y_batch):
        model.fit(X_batch,Y_batch,
                        epochs=consts.num_epoches,
                        batch_size=consts.keras_batch_size,
                        shuffle=True,
                        verbose=True)
        return model

def save_trained_classifier(model):
    model.save(consts.sequential_model_path)

def train_classifier():
        train,test = prepare_inputs.loadSplit()
        np.random.shuffle(train)
        train_batches = np.array_split(train,len(train)/consts.train_batch_size)

        print("Compiling model...")
        model = create_model()

        batch_number = consts.train_batch_number
        if batch_number < 0:
            batch_number = len(train_batches)

        for i in range(batch_number):
            batch = train_batches[i]
            print("Running batch "+ str(i+1) + "/"+ str(batch_number) +".")
            X_batch,Y_batch = prepare_inputs.prepare_batch(batch,type = consts.feature_type,sub_type = consts.sub_type)
            print("Inputs loaded.")
            model = train_classifier_batch(model,X_batch,Y_batch)

        print("Classifier model is trained successfully.")
        save_trained_classifier(model)
