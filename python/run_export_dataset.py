import numpy as np
import pandas as pd

from consts import *
from features import prepare_inputs
from features.models_inception_features import initiate_inception_network
from mixed_features.utils import *

consts = get_consts()

def save_csv(data,path):
    pd.DataFrame(data).to_csv(path,header=None,index=None)

def export_dataset():

        print("Loading splits...")
        #train,test = prepare_inputs.loadSplit()
        trainData,trainLabels,trainMapData,testData,testLabels,testMapData = load_structure_features()
        np.random.shuffle(train)
        test_batches = np.array_split(test,len(test)/consts.test_batch_size)
        train_batches = np.array_split(train,len(train)/consts.train_batch_size)
        print("Done...")

        print("Loading model...")
        encode_fn = initiate_inception_network()

        batch_number = consts.train_batch_number
        if batch_number < 0:
            batch_number = len(train_batches)
        X_train = []
        Y_train = []
        X_test = []
        Y_test = []
        for i in range(batch_number):
            batch = train_batches[i]
            print("Running batch "+ str(i+1) + "/"+ str(batch_number) +".")
            X_batch,Y_batch = prepare_inputs.prepare_batch(batch,type = consts.feature_type,sub_type = consts.sub_type,
                                                           encode_fn=encode_fn)
            X_train += X_batch.tolist()
            Y_train += Y_batch.tolist()

        for i in range(batch_number):
            batch = test_batches[i]
            print("Running batch " + str(i + 1) + "/" + str(batch_number) + ".")
            X_batch, Y_batch = prepare_inputs.prepare_batch(batch, type=consts.feature_type,
                                                            sub_type=consts.sub_type,encode_fn=encode_fn)
            X_test += X_batch.tolist()
            Y_test += Y_batch.tolist()

        print("Data set is created successfully.")
        save_csv(X_train,"raw_data/export/trainData.csv")
        save_csv(Y_train,"raw_data/export/trainLabels.csv")
        save_csv(X_test,"raw_data/export/testData.csv")
        save_csv(Y_test,"raw_data/export/testLabels.csv")
export_dataset()