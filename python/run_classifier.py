from keras.models import load_model
from consts import *
from features.prepare_inputs import *
from sklearn.metrics import accuracy_score

consts = get_consts()

def load_trained_classifier():
    model = load_model(consts.sequential_model_path)
    return model

def run_classifier(X_batch):
    model = load_trained_classifier()
    return model.predict(X_batch)


def evaluate_classifier():
    train,test = loadSplit()
    model = load_trained_classifier()

    test_batches = np.array_split(test, len(test) / consts.test_batch_size)

    y_test = []
    y_pred = []
    preds_list = []

    batch_number = consts.test_batch_number
    if batch_number < 0:
        batch_number = len(test_batches)

    for i in range(batch_number):
        batch = test_batches[i]
        print("Running batch "+ str(i+1) + "/"+ str(batch_number) +".")
        x_batch,y_batch = prepare_batch(batch,type=2,sub_type = consts.sub_type)
        preds = np.concatenate(model.predict(x_batch))
        preds_list += preds.tolist()
        y_pred += (preds+0.5).astype(int).tolist()
        y_test += y_batch.tolist()
    test_accuracy = (accuracy_score(y_test, y_pred))
    print(preds_list,y_pred,y_test)
    print(test_accuracy)
