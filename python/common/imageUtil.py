import numpy as np


def crop(img,mask,padding):
    inds = np.where(mask)
    ys =  [ ind[0] for ind in inds]
    xs =  [ ind[1] for ind in inds]
    miny = np.max((np.min(ys) - padding,0))
    minx = np.max((np.min(xs) - padding,0))
    maxy = np.min((np.max(ys) + padding,img.shape[0]-1))
    maxx = np.min((np.max(xs) + padding,img.shape[1]-1))
    return img[miny:maxy+1, minx:maxx+1]