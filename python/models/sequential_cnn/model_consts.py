
def get_consts(consts):

    # Model specific training configuration
    consts.train_batch_size = 16*20
    consts.train_batch_number = -1
    consts.test_batch_size = 100
    consts.test_batch_number = -1
    consts.num_epoches = 30
    consts.keras_batch_size = 32

    # Necessary attributes:
    consts.model_type = ""
    consts.sub_model = 0

    return consts
