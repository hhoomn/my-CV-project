import pandas as pd
import numpy as np
from consts import *
from models.resnet50.models import *
from mixed_features.utils import load_image_data,load_structure_labels
from mixed_features.utils import *
from common.imageUtil import *
from matplotlib import pyplot
import os
from matplotlib import pyplot

consts = get_consts()

def initiate_resnet50_network():
    net = create_resnet50_model()
    net = load_resnet_weights(net, consts.resnet_weight_path)
    encode_fn = create_functions(net)
    return encode_fn

def prepare_input(img,mask,sub_type):
    if sub_type == 1 :
        out = img*mask
    elif sub_type == 2:
        out = crop(img,mask,padding=10)
    return preprocess(out)

def load_features_batch_type7(index,regions,st_features,labels,encode_fn,sub_type):
    print("loading type 5 features for image "+ str(index)+"...")
    depth,rgb,_,supports,imgRegions = load_image_data(index)
    feature_list = []
    feature_labels = []
    for i in range(len(regions)):
        (img, region, label, strctr) = regions[i]
        if region == 0:
            continue
        region_mask = (imgRegions == region)
        priliminary_features = rgb
        features=np.concatenate([encode_fn(prepare_input(priliminary_features,region_mask,sub_type)).reshape((2048,)),st_features[i]])
        label = labels[i]
        assert(label == strctr)
        feature_labels = feature_labels+[label]
        feature_list = feature_list+[features]
    return feature_list,feature_labels


def prepare_batch_type7(region_batch,st_features,labels,sub_type,encode_fn):
    print("Running batch with lenght:"+ str(len(region_batch))+"...")
    batch_features = []
    batch_labels = []
    sub_batchs = group_regions_by_image(region_batch,st_features,labels)
    for (regions,sub_features,sub_labels) in sub_batchs:
        features,labels = load_features_batch_type7(regions[0][0],regions,sub_features,sub_labels,encode_fn,sub_type)
        batch_features+= features
        batch_labels+=labels
    batch_features = np.stack(batch_features)
    return batch_features,np.array(batch_labels)


def feature_consts(consts):
    consts.structure_feature_dim = 1128
    consts.feature_shape = (4096+consts.structure_feature_dim,)
    consts.feature_type = 7
    consts.sub_type = 1
    return consts