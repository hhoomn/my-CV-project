import pandas as pd
import numpy as np
from consts import *
from mixed_features.plus_inception_features import *
from mixed_features.plus_resnet_features import *
import os
from matplotlib import pyplot

consts = get_consts()


def prepare_batch(batch, st_features=[], labels=[], type=5,sub_type=1,encode_fn = None):
    if(type == 5):
        return prepare_batch_type5(batch,st_features,labels,sub_type,encode_fn)
    elif type == 7:
        return prepare_batch_type7(batch,st_features,labels,sub_type,encode_fn)

def initiate_network(type = 5):
    if(type == 5):
        encode_fn = initiate_inception_network()
    elif type == 7:
        encode_fn = initiate_resnet50_network()
    return encode_fn

def generateSplit(num = 1449, p = 0.2):
    test=np.random.binomial(1,p,num)
    pd.DataFrame(test).to_csv(consts.split_file,index=False,header=False)

def loadStructureDataset():
    trainData= pd.read_csv(consts.st_train_data , header=None).values
    trainLabels= pd.read_csv(consts.st_train_labels , header=None).values.astype(int)
    trainMapData= pd.read_csv(consts.st_train_map_data , header=None).values.astype(int)
    testData= pd.read_csv(consts.st_test_data , header=None).values
    testLabels= pd.read_csv(consts.st_test_labels , header=None).values.astype(int)
    testMapData= pd.read_csv(consts.st_test_map_data , header=None).values.astype(int)
    return trainData,trainLabels,trainMapData,testData,testLabels,testMapData
