
import pandas as pd
import numpy as np
from consts import *

import os
from matplotlib import pyplot

consts = get_consts()
consts.crop_mask = np.zeros((480, 640))
consts.crop_mask[44:471, 40:601] = 1
consts.crop_mask = consts.crop_mask.astype(bool)
consts.crop_sz = (427, 561)
consts.cropped_property_shape = (427,561,1)


###
#  Loading image components

def channel_normalize(ndarray):
    return (ndarray - np.average(ndarray)) / np.max(ndarray)

def channel_crop(data):
    sz = consts.crop_sz
    mask = consts.crop_mask
    if len(data.shape) <= 2:
        return  data[mask].reshape((sz[0],sz[1],1))
    else:
        return data.reshape((480*640,data.shape[2]))[mask.flatten(),:].reshape((sz[0],sz[1],data.shape[2]))

def load_channel(path,index,shape = consts.property_shape):
    return channel_crop(pd.read_csv(path % index, header=None).values.reshape(shape))

def load_image_data(index):
    image_depth = channel_normalize(load_channel(consts.image_depth_file,index))
    image_rgb_raw = pd.read_csv(consts.image_rgb_file % index, header=None).values*(consts.image_value_scale)
    image_rgb = np.stack([image_rgb_raw[:, i * consts.width:(i + 1) * consts.width] for i in range(3)], -1)
    image_rgb = channel_crop(image_rgb)
    image_label = load_channel(consts.label_file,index)
    image_regions = pd.read_csv(consts.region_file % index, header=None).values.reshape(consts.cropped_property_shape).astype(int) #load_channel(consts.region_file,index,shape=consts.crop_sz) #pd.read_csv(consts.region_file % index, header=None).values #.reshape(consts.property_shape)
    support_labels= pd.read_csv(consts.support_label_file % index, header=None).values
    return image_depth,image_rgb,image_label,support_labels,image_regions

###

def load_structure_labels():
    structure_labels= np.concatenate(pd.read_csv(consts.structure_labels_path , header=None).values.astype(int))
    return structure_labels

###
#  Region Utility

def group_regions_by_image(region_batch,st_features,labels):
    sub_batchs = []
    last_sub_batch = []
    last_sub_features = []
    last_sub_labels = []

    for i in range(len(region_batch)):
        region = region_batch[i]
        st_feature = st_features[i]
        label = labels[i]
        if len(last_sub_batch) < 1 or region[0] == last_sub_batch[0][0]:
            last_sub_batch += [region]
            last_sub_features += [st_feature]
            last_sub_labels += [label]
        else:
            sub_batchs += [(last_sub_batch,last_sub_features,last_sub_labels)]
            last_sub_batch = [region]
            last_sub_features = [st_feature]
            last_sub_labels = [label]

    if len(last_sub_batch) > 0:
        sub_batchs += [(last_sub_batch,last_sub_features,last_sub_labels)]
    return sub_batchs


# def load_map_data():
#     return map_data
# if (not os.path.exists(consts.dataset_dir))
#     mkdir
