import pandas as pd
import numpy as np
from consts import *
from features.relationship_type_features import *
from features.relationship_features import *
from features.structure_class_features import *
from features.models_inception_features import *
import os
from matplotlib import pyplot

consts = get_consts()


def prepare_batch(batch,type=4,sub_type=0,encode_fn = None):
    if(type == 1):
        return prepare_batch_type1(batch)
    elif(type == 2):
        return prepare_batch_type2(batch,sub_type)
    elif(type == 3):
        return prepare_batch_type3(batch,sub_type)
    elif(type == 4):
        return prepare_batch_type4(batch,sub_type,encode_fn)

def generateSplit(num = 1449, p = 0.2):
    test=np.random.binomial(1,p,num)
    pd.DataFrame(test).to_csv(consts.split_file,index=False,header=False)

def loadSplit():
    test_boolean = np.array(pd.read_csv(consts.split_file, header=None)).reshape((consts.imageNum,)) == 1
    all = np.array(range(1,consts.imageNum+1))
    test = all[test_boolean]
    train = all[ test_boolean == False]
    return train,test
