
import pandas as pd
import numpy as np
from consts import *

import os
from matplotlib import pyplot

consts = get_consts()

def channel_normalize(ndarray):
    return (ndarray - np.average(ndarray)) / np.max(ndarray)


def load_image_data(index):
    image_depth = channel_normalize(pd.read_csv(consts.image_depth_file % index, header=None).values.reshape(consts.property_shape))
    image_rgb_raw = pd.read_csv(consts.image_rgb_file % index, header=None).values*(consts.image_value_scale)
    image_rgb = np.stack([image_rgb_raw[:, i * consts.width:(i + 1) * consts.width] for i in range(3)], -1)
    image_label = pd.read_csv(consts.label_file % index, header=None).values.reshape(consts.property_shape)
    support_labels= pd.read_csv(consts.support_label_file % index, header=None).values
    return image_depth,image_rgb,image_label,support_labels


def load_structure_labels():
    structure_labels= np.concatenate(pd.read_csv(consts.structure_labels_path , header=None).values.astype(int))
    return structure_labels

def load_map_data():
    structure_labels= np.concatenate(pd.read_csv(consts.structure_labels_path , header=None).values.astype(int))
    return structure_labels
# if (not os.path.exists(consts.dataset_dir))
#     mkdir
