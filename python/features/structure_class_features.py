import pandas as pd
import numpy as np
from consts import *
from features.utils import load_image_data,load_structure_labels
import os
from matplotlib import pyplot

consts = get_consts()


def load_features_type3(index):
    print("loading type 3 features for image "+ str(index)+"...")
    depth,rgb,labels,supports = load_image_data(index)
    structure_labels = load_structure_labels() #TODO double check indexing
    regions = np.unique(labels)
    depth = depth.reshape((consts.image_shape[0],consts.image_shape[1],1))
    feature_list = []
    feature_labels = []
    for region in regions:
        if region == 0:
            continue
        region_mask = (labels == region)
        features = rgb*region_mask
        label = structure_labels[region-1]
        feature_labels = feature_labels+[label]
        feature_list = feature_list+[features]
    return feature_list, feature_labels


def prepare_batch_type3(batch):
    batch_features = []
    batch_labels = []
    for index in batch:
        features, labels = load_features_type3(index)
        batch_features += features
        batch_labels += labels
    batch_features = np.stack(batch_features)
    return batch_features,np.array(batch_labels)

def feature_consts(consts):
    consts.feature_type = 3
    consts.sub_type = 0
    consts.feature_shape = (480,640,3)
    consts.sample_per_image = 2

    return consts