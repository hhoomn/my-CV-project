import pandas as pd
import numpy as np
from consts import *
from features.utils import load_image_data
import os
from matplotlib import pyplot

consts = get_consts()

def process_type2_features(image_data,sub_type=2):
    if sub_type == 1:
        return np.concatenate(image_data,axis=2)
    if sub_type == 2:
        new_image_data = (image_data[0],np.sum(image_data[1],axis=2).reshape(consts.property_shape),image_data[2],image_data[3])
        return np.concatenate(new_image_data,axis=2)
    if sub_type == 3:
        new_image_data = (image_data[0],np.sum(image_data[1],axis=2).reshape(consts.property_shape),(image_data[2]-image_data[3]).reshape(consts.property_shape))
        return np.concatenate(new_image_data,axis=2)
    if sub_type == 4:
        new_image_data = (image_data[0],image_data[2],image_data[3])
        return np.concatenate(new_image_data,axis=2)



def load_features_type2(index,sub_type = 2):
    print("loading type 2("+str(sub_type)+") features for image "+ str(index)+"...")
    depth,rgb,labels,supports = load_image_data(index)
    depth = depth.reshape((consts.image_shape[0],consts.image_shape[1],1))
    feature_list = []
    feature_labels = []
    max_region = np.max(supports[:,0:2])
    region_pairs = [ (i,j) for i in range(1,max_region+1) for j in range(0,max_region+1)]
    no_rel_pairs = region_pairs
    for support in supports:
        if(support[1] < 0):
            continue
        first_object = (labels == support[0]).astype(int)
        second_object = (labels == support[1]).astype(int)
        if support[1] > 0 and (support[0],support[1]) in no_rel_pairs :
            no_rel_pairs.remove((support[0],support[1]))
        features = process_type2_features((depth,rgb,first_object,second_object),sub_type=sub_type)
        label = 1
        feature_labels = feature_labels+[label]
        feature_list = feature_list+[features]
    np.random.shuffle(no_rel_pairs)
    for (x,y) in no_rel_pairs[0:len(supports)]:
        first_object = (labels == x).astype(int)
        second_object = (labels == y).astype(int)
        features = process_type2_features((depth,rgb,first_object,second_object),sub_type=sub_type)
        label = 0
        feature_labels = feature_labels + [label]
        feature_list = feature_list + [features]
    sample_per_image = consts.sample_per_image
    inds = np.array(range(len(feature_list)))
    np.random.shuffle(inds)
    shuffled_feature_list = list(map((lambda x: feature_list[x]),inds))
    shuffled_feature_labels = list(map((lambda x: feature_labels[x]),inds))
    if sample_per_image > 0:
        shuffled_feature_list = shuffled_feature_list[0:sample_per_image]
        shuffled_feature_labels = shuffled_feature_labels[0:sample_per_image]
        # print("sampling "+str(sample_per_image)+" random relation(s) from image.")
    return shuffled_feature_list,shuffled_feature_labels

def prepare_batch_type2(batch,sub_type = 2):
    batch_features = []
    batch_labels = []
    for i in range(len(batch)):
        index = batch[i]
        features,labels = load_features_type2(index,sub_type)
        batch_features+= features
        batch_labels+=labels
        print("Loaded " + str(i+1) + "/" + str(len(batch)) + " images.")
    batch_features = np.stack(batch_features)
    return batch_features,np.array(batch_labels)


def feature_consts(consts):
    # Configure this parameter for sub_types
    consts.sub_type = 1

    consts.feature_type = 2
    consts.feature_type = (480,640,6)
    consts.sample_per_image = 2

    if consts.sub_type == 1:
        consts.feature_shape = (480,640,6)
    elif consts.sub_type == 2:
        consts.feature_shape = (480,640,4)
    elif consts.sub_type == 3:
        consts.feature_shape = (480,640,3)
    elif consts.sub_type == 4:
        consts.feature_shape = (480,640,3)

    return consts
