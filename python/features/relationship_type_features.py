import pandas as pd
import numpy as np
from consts import *
from features.prepare_inputs import *
import os
from matplotlib import pyplot

consts = get_consts()


def load_features_type1(index):
    print("loading type 1 features for image "+ str(index)+"...")
    depth,rgb,labels,supports = load_image_data(index)
    depth = depth.reshape((consts.image_shape[0],consts.image_shape[1],1))
    feature_list = []
    feature_labels = []
    for support in supports:
        first_object = (labels == support[0]).astype(int)
        second_object = (labels == support[1]).astype(int)
        features = np.concatenate((depth,rgb,first_object,second_object),2)
        label = support[2]
        feature_labels = feature_labels+[label]
        feature_list = feature_list+[features]
    return feature_list,feature_labels


def prepare_batch_type1(batch):
    batch_features = []
    batch_labels = []
    for index in batch:
        features,labels = load_features_type1(index)
        batch_features+= features
        batch_labels+=labels
    batch_features = np.stack(batch_features)
    return batch_features,np.array(batch_labels)


def feature_consts(consts):
    consts.feature_shape = (480,640,6)
    consts.feature_type = 1
    consts.sub_type = 0
    return consts
