import pandas as pd
import numpy as np
from consts import *
from models.resnet50.models import *
from features.utils import load_image_data,load_structure_labels
from common.imageUtil import *
from matplotlib import pyplot
import os
from matplotlib import pyplot

consts = get_consts()

def initiate_resnet50_network():
    net = create_resnet50_model()
    net = load_resnet_weights(net, consts.resnet_weight_path)
    encode_fn = create_functions(net)
    return encode_fn

def prepare_input(img,mask,sub_type):
    if sub_type == 1 :
        out = img*mask
    elif sub_type == 2:
        out = crop(img,mask,padding=10)
    return preprocess(out)

def load_features_type6(index,encode_fn,sub_type):
    print("loading type 4 features for image "+ str(index)+"...")
    depth,rgb,labels,supports = load_image_data(index)
    structure_labels = load_structure_labels() #TODO double check indexing
    regions = np.unique(labels)
    depth = depth.reshape((consts.image_shape[0],consts.image_shape[1],1))
    feature_list = []
    feature_labels = []
    for region in regions:
        if region == 0:
            continue
        region_mask = (labels == region)
        priliminary_features = rgb
        features=encode_fn(prepare_input(priliminary_features,region_mask,sub_type))
        label = structure_labels[region-1]
        feature_labels = feature_labels+[label]
        feature_list = feature_list+[features.reshape((2048,))]
    return feature_list,feature_labels

def prepare_batch_type6(batch,sub_type,encode_fn):
    print("Running batch with lenght:"+ str(len(batch))+"...")
    batch_features = []
    batch_labels = []
    for index in batch:
        features,labels = load_features_type6(index,encode_fn,sub_type)
        batch_features+= features
        batch_labels+=labels
    batch_features = np.stack(batch_features)
    return batch_features,np.array(batch_labels)


def feature_consts(consts):
    consts.feature_shape = (2048,)
    consts.feature_type = 4
    consts.sub_type = 0
    return consts
