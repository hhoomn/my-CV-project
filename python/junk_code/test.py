import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
import numpy as np
from numpy import ravel
from keras.layers import Input
from keras.models import Model
from keras.models import Sequential
from keras.layers import Dense, Activation
from sklearn import svm
from sknn import mlp
#from matplotlib import pyplot
from sklearn.model_selection import cross_val_score


trainDataMain = pd.read_csv("data/trainData.csv")
trainLabelsColumn = pd.read_csv("data/trainLabels.csv")
testData = pd.read_csv("data/testData.csv")
testLabelsColumn= pd.read_csv("data/testLabels.csv")

trainLabelsMain = ravel(trainLabelsColumn)
testLabels = ravel(testLabelsColumn)

train_sizes = range(18000,np.size(trainLabelsMain),2000)
accTrains = []
accTests = []

for train_size in train_sizes:
    print("Applying ")
    trainData = trainDataMain[0:train_size]
    trainLabels = trainLabelsMain[0:train_size]
    method = 4
    if method == 1:

        clf = RandomForestClassifier()

        #scores = cross_val_score(clf,trainData,trainLabels)
        clf.fit(trainData,trainLabels)
        trainPred = clf.predict(trainData)
        train_accuracy=(accuracy_score(trainPred,trainLabels))

        testPred = clf.predict(testData)
        test_accuracy=(accuracy_score(testPred,testLabels))
    elif method == 2:
        print(pd.DataFrame(trainData).shape)
        input_dim = trainData.shape[1]
        encoding_dim = 256
        hidden_dim = 90
        input_buf = Input(shape=(input_dim,))
        encoded = Dense(encoding_dim, activation="relu")(input_buf)
        decoded = Dense(input_dim, activation="sigmoid")(encoded)
        autoencoder = Model(input_buf, decoded)
        encoder = Model(input_buf, encoded)
        autoencoder.compile(optimizer='adadelta', loss='binary_crossentropy')
        autoencoder.fit(trainData.values, trainData.values,
                        epochs=50,
                        batch_size=256,
                        shuffle=True,
                        validation_data=(trainData.values, trainData.values))

        print("Learning classifier")

        multilp = Sequential([
            Dense(hidden_dim, input_shape=(encoding_dim,)),
            Activation('relu'),
            Dense(4),
            Activation('softmax'),
        ])

        trainOneHot = map((lambda x: [0 if i != (x - 1) else 1 for i in range(4)]), trainLabels)

        encodedTrainData = encoder.predict(trainData.values)
        encodedTestData = encoder.predict(testData.values)

        multilp.compile(optimizer='rmsprop',
                        loss='categorical_crossentropy',
                        metrics=['accuracy'])

        multilp.fit(encodedTrainData, trainOneHot)

        trainPred = multilp.predict_classes(encodedTrainData)
        testPred = multilp.predict_classes(encodedTestData)

        train_accuracy = (accuracy_score(trainPred, trainLabels))
        test_accuracy = (accuracy_score(testPred, testLabels))
    elif method == 3:
        print(pd.DataFrame(trainData).shape)
        input_dim = trainData.shape[1]
        encoding_dim = 30
        hidden_dim = 10
        input_buf = Input(shape= (input_dim,))
        encoded = Dense(encoding_dim,activation="relu")(input_buf)
        decoded = Dense(input_dim, activation="sigmoid")(encoded)
        autoencoder = Model(input_buf,decoded)
        encoder = Model(input_buf,encoded)
        autoencoder.compile(optimizer='adadelta', loss='binary_crossentropy')
        autoencoder.fit(trainData.values, trainData.values,
                        epochs=10,
                        batch_size=256,
                        shuffle=True,
                        validation_data=(trainData.values, trainData.values))

        print("Learning classifier")

        multilp = Sequential([
            Dense(hidden_dim, input_shape=(encoding_dim,)),
            Activation('relu'),
            Dense(4),
            Activation('softmax'),
        ])

        trainOneHot=map((lambda x: [0 if i != (x-1) else 1 for i in range(4)]),trainLabels)

        encodedTrainData = encoder.predict(trainData.values)
        encodedTestData = encoder.predict(testData.values)

        multilp.compile(optimizer='rmsprop',
                      loss='categorical_crossentropy',
                      metrics=['accuracy'])

        multilp.fit(encodedTrainData,trainOneHot)

        trainPred=multilp.predict_classes(encodedTrainData)
        testPred=multilp.predict_classes(encodedTestData)

        train_accuracy=(accuracy_score(trainPred,trainLabels))
        test_accuracy=(accuracy_score(testPred,testLabels))
    elif method == 4:
        clf = svm.SVC()

        #scores = cross_val_score(clf,trainData,trainLabels)
        clf.fit(trainData,trainLabels)
        trainPred = clf.predict(trainData)
        train_accuracy=(accuracy_score(trainPred,trainLabels))

        testPred = clf.predict(testData)
        test_accuracy=(accuracy_score(testPred,testLabels))

    accTrains += [train_accuracy]
    accTests += [test_accuracy]

print(train_size)
print(accTrains)
print(accTests)
#pyplot.plot(train_sizes,accTrains,"r",train_sizes,accTests,"b")
#pyplot.show()
#print(scores.mean())