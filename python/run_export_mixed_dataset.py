import numpy as np
import pandas as pd

from consts import *
from mixed_features.prepare_inputs import *
from features.models_resnet50_features import initiate_resnet50_network
from mixed_features.utils import *
from mixed_features import prepare_inputs
import mixed_features

consts = get_consts()

def save_csv(data,path):
    pd.DataFrame(data).to_csv(path,header=None,index=None)

def export_dataset():

        print("Loading splits...")
        #train,test = prepare_inputs.loadSplit()
        trainData,trainLabels,trainMapData,testData,testLabels,testMapData = loadStructureDataset()
        test = range(len(testData))
        train = range(len(trainData))
        test_batches = np.array_split(test,len(test)/consts.test_batch_size)
        train_batches = np.array_split(train,len(train)/consts.train_batch_size)
        print("Done...")

        print("Loading model...")
        encode_fn = initiate_network(type = consts.feature_type)

        batch_number = consts.train_batch_number
        if batch_number < 0:
            batch_number = len(train_batches)
        X_train = []
        Y_train = []
        map_train = []
        X_test = []
        Y_test = []
        map_test = []
        batch_list = range(batch_number)
        for i in batch_list:
            batch = train_batches[i]
            regions = trainMapData[batch]
            st_features = trainData[batch]
            labels = trainLabels[batch]

            print("Running batch "+ str(i+1) + "/"+ str(batch_number) +".")
            X_batch,Y_batch = prepare_batch(batch=regions,st_features=st_features,labels=labels,type = consts.feature_type,sub_type = consts.sub_type,
                                                           encode_fn=encode_fn)
            X_train += X_batch.tolist()
            Y_train += Y_batch.tolist()
            map_train += (regions).tolist()

        batch_number = len(test_batches)
        batch_list = range(batch_number)
        for i in batch_list:
            batch = test_batches[i]
            regions = testMapData[batch]
            st_features = testData[batch]
            labels = testLabels[batch]
            print("Running batch " + str(i + 1) + "/" + str(batch_number) + ".")
            X_batch, Y_batch = prepare_batch(regions, st_features, labels, type=consts.feature_type,
                                                            sub_type=consts.sub_type,encode_fn=encode_fn)
            X_test += X_batch.tolist()
            Y_test += Y_batch.tolist()
            map_test += regions.tolist()

        print("Data set is created successfully.")
        save_csv(X_train,"raw_data/export/trainData.csv")
        save_csv(Y_train,"raw_data/export/trainLabels.csv")
        save_csv(map_train,"raw_data/export/trainMapData.csv")
        save_csv(X_test,"raw_data/export/testData.csv")
        save_csv(Y_test,"raw_data/export/testLabels.csv")
        save_csv(map_test,"raw_data/export/testMapData.csv")
export_dataset()