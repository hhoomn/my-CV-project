import mock

#import a model and a feature back_end
from models.resnet50 import model_consts

def feature_consts_5(consts):
    consts.structure_feature_dim = 1128
    consts.feature_shape = (2048+consts.structure_feature_dim,)
    consts.feature_type = 5
    consts.sub_type = 1
    return consts

def feature_consts_7(consts):
    consts.structure_feature_dim = 1128
    consts.feature_shape = (4096+consts.structure_feature_dim,)
    consts.feature_type = 7
    consts.sub_type = 1
    return consts


def get_consts():
    consts = mock.Mock()
    # Data path configuration
    consts.split_file = "data/train_test_split.csv"

    consts.image_dir = "raw_data/images/"
    consts.image_depth_file = consts.image_dir+"depth_%0.6d.csv"
    consts.image_rgb_file = consts.image_dir+"rgb_%0.6d.csv"

    consts.label_dir = "raw_data/labels/"
    consts.label_file = consts.label_dir+"labels_%0.6d.csv"
    consts.support_label_file = consts.label_dir+"supportLabels_%0.6d.csv"
    consts.region_file = consts.label_dir+"regions_%0.6d.csv"
    consts.structure_labels_path = consts.label_dir+"structureLabels.csv"

    consts.model_dir = "data/models/"
    consts.sequential_model_path = consts.model_dir+"sequential_model.h5"
    consts.inception_weight_path = consts.model_dir+"inception_v3.pkl"
    consts.resnet_weight_path = consts.model_dir+"resnet50.pkl"
    consts.c3d_weight_path = consts.model_dir+"c3d_model.pkl"

    consts.export_dir = "data/export/"

    consts.structure_dataset_dir = "data/structure_classifier_dataset/"
    consts.st_train_data = consts.structure_dataset_dir + "trainData.csv"
    consts.st_train_labels = consts.structure_dataset_dir + "trainLabels.csv"
    consts.st_train_map_data = consts.structure_dataset_dir + "trainMapData.csv"
    consts.st_test_data = consts.structure_dataset_dir + "testData.csv"
    consts.st_test_labels = consts.structure_dataset_dir + "testLabels.csv"
    consts.st_test_map_data = consts.structure_dataset_dir + "testMapData.csv"

    consts.imageNum = 1449
    consts.width = 640

    # Normalization configuration
    consts.image_value_scale = 1.0/255
    consts.depth_value_scale = 1.0/4

    # Default model training configuration, can be overridden in model consts
    consts.train_batch_size = 16*20
    consts.train_batch_number = -1
    consts.test_batch_size = 10
    consts.test_batch_number = -1
    consts.num_epoches = 30
    consts.keras_batch_size = 32

    # Default feature configuration, can be overridden in feature consts
    consts.image_shape = (480,640,3)
    consts.property_shape = (480,640,1)
    consts.feature_shape = (480,640,6)

    # Default feature type configuration
    consts.feature_type = 1
    consts.sub_type = 0
    consts.sample_per_image = 2

    # Using model/feature specific configuration
    consts = model_consts.get_consts(consts)
    consts = feature_consts_7(consts)
    return consts
